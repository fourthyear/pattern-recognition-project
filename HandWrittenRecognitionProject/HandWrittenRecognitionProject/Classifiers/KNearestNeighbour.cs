﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HandWrittenRecognitionProject.Classifiers
{
    public class KNearestNeighbour
    {
        private int numberOfClasses;
        private byte[][] trainingImagesFeatures;
        private byte[] trainingLabels;

        private List<double> nearestNeighboursDistances;
        private List<int> nearestNeighboursClasses;

        private int[] classesFrequency;
        private double[][] classesMeans;

        public KNearestNeighbour(int numberOfClasses, byte[][] trainingImagesFeatures, byte[] trainingLabels)
        {
            this.numberOfClasses = numberOfClasses;
            this.trainingImagesFeatures = trainingImagesFeatures;
            this.trainingLabels = trainingLabels;

            
        }

        public int kNearestNeighborClassifier(int K, byte[] testImageFeatures)
        {
            int[] classFrequency = new int[this.numberOfClasses];
            int classIndex = 0;
            int maximumOccure = 0;

            this.nearestNeighboursClasses = new List<int>();
            this.nearestNeighboursDistances = new List<double>();
            List<KeyValuePair<double, int>> neighbours = new List<KeyValuePair<double, int>>();

            #region find KNN

            for (int index = 0; index < trainingImagesFeatures.Count(); index++)
            {
                double distance = Utilities.Euclidean(testImageFeatures, trainingImagesFeatures[index]);

                neighbours.Add(new KeyValuePair<double, int>(distance, trainingLabels[index]));
            }
            //sorting 3ala l Key
            neighbours = neighbours.OrderBy(x => x.Key).ToList();

            for (int i = 0; i < K; i++)
            {
                this.nearestNeighboursDistances.Add(neighbours[i].Key);
                this.nearestNeighboursClasses.Add(neighbours[i].Value);
            }

            #endregion
            // 1 3 4 1 3
            int idx = 0;

            #region Class set
            for (int i = 0; i < nearestNeighboursClasses.Count; i++)
            {
                classFrequency[nearestNeighboursClasses[i]]++;

                if (++idx == K) break;
            }

            for (int i = 0; i < this.numberOfClasses; i++)
            {
                if (classFrequency[i] > maximumOccure)
                {
                    classIndex = i;
                    maximumOccure = classFrequency[i];
                }
            }

            return classIndex;

            #endregion

        }

        public void preprocessing()
        {   
            this.classesMeans = new double[this.numberOfClasses][];

            for (int i = 0; i < this.numberOfClasses; i++)
            {
                this.classesMeans[i] = new double[28 * 28];
            }

            this.classesFrequency = new int[this.numberOfClasses];

            for (int i = 0; i < this.trainingLabels.Count(); i++)
            {
                this.classesFrequency[this.trainingLabels[i]]++;

                for (int j = 0; j < this.trainingImagesFeatures[i].Count(); j++)
                {
                    this.classesMeans[this.trainingLabels[i]][j] += this.trainingImagesFeatures[i][j];
                }
            }

            for (int i = 0; i < this.numberOfClasses; i++)
            {
                for (int j = 0; j < 28 * 28; j++)
                {
                    this.classesMeans[i][j] = this.classesMeans[i][j] / this.classesFrequency[i];
                }
            }
        }

        public int nearestCentroidClassifier(byte[] testImageFeatures)
        {
            double minimumDistance = 0.0;

            int predictedClass = 0;

            double distance = 0.0;

            for (int i = 0; i < classesMeans.Length; i++)
            {
                distance = Utilities.EuclideanDouble(classesMeans[i], testImageFeatures);

                if (i == 0)
                {
                    minimumDistance = distance;
                    predictedClass = i;
                }

                else
                {
                    if (distance < minimumDistance)
                    {
                        minimumDistance = distance;
                        predictedClass = i;
                    }
                }
            }

            return predictedClass;
        }
    }
}
